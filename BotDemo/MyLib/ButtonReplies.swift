//
//  CustomButton.swift
//  DrawableExample
//
//  Created by Swapnil Dhavan on 07/01/19.
//  Copyright © 2019 Test. All rights reserved.
//

import UIKit

class ButtonReplies: UIButton {
    
    override func awakeFromNib() {
        super.awakeFromNib()
       self.layer.masksToBounds = false
        //self.layer.cornerRadius = frame.height/2
        self.layer.cornerRadius = frame.height/2
        self.layer.shadowColor = UIColor.black.cgColor
        self.layer.shadowOffset = CGSize(width: 0, height: 3)
        self.layer.shadowOpacity = 0.40
        self.layer.shadowRadius = 2
        self.layer.borderWidth = 1
        self.layer.borderColor = Constants.shared.hexStringToUIColor(hex: (Pref.getPref(key: Constants.shared.THEME_COLOR) as! String)).cgColor
    }
}
