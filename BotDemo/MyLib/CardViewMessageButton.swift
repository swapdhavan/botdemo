//
//  CardView.swift
//  DrawableExample
//
//  Created by Alap Soni on 20/09/18.
//  Copyright © 2018 Test. All rights reserved.
//

import UIKit

@IBDesignable class CardViewMessageButton: UIView {

    @IBInspectable var cornerradius : CGFloat = 10
    @IBInspectable var shadowOffSetWidth : CGFloat = 0
    @IBInspectable var shadowOffSetHeight : CGFloat = 5
    @IBInspectable var shadowColor : UIColor = UIColor.black
    @IBInspectable var shadowOpacity : CGFloat = 0.5
    @IBInspectable var type : Int = 0
    
   
    override func layoutSubviews() {
        
        var maskPath = UIBezierPath()
        let maskLayer = CAShapeLayer()
        
        /*if type == 1{
        maskPath = UIBezierPath(roundedRect: bounds,
                                    byRoundingCorners: [.topLeft, .bottomLeft, .bottomRight],
                                    cornerRadii: CGSize(width: frame.height/2, height: frame.height/2))
        }else{
        maskPath = UIBezierPath(roundedRect: bounds,
        byRoundingCorners: [.topRight, .bottomLeft, .bottomRight],
        cornerRadii: CGSize(width: frame.height/2, height: frame.height/2))
        }*/
        
        maskPath = UIBezierPath(roundedRect: bounds,
        byRoundingCorners: [.topRight, .bottomLeft, .bottomRight],
        cornerRadii: CGSize(width: 50, height: 50))
        
        maskLayer.path = maskPath.cgPath
        
        layer.shadowColor = shadowColor.cgColor
        layer.shadowOffset = CGSize(width: shadowOffSetWidth,height: shadowOffSetHeight)
        //let shadowPath = UIBezierPath(roundedRect: bounds, cornerRadius: frame.height/2)
        layer.shadowPath = maskPath.cgPath
        //layer.shouldRasterize = true
        //layer.rasterizationScale = scale ? UIScreen.main.scale : 1
        layer.shadowOpacity = Float(shadowOpacity)
        layer.mask = maskLayer
    }
}
