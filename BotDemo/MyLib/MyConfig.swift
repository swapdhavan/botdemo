//
//  MyConfig.swift
//  DrawableExample
//
//  Created by Alap Soni on 02/10/18.
//  Copyright © 2018 Test. All rights reserved.
//

import Foundation
struct MyConfig {
    static let URL = "https://aianalytics.locobuzz.com/webhook/"
    
    
    static let SEND_MESSAGE = URL+"WebsiteMessage"
    static let LOGIN = URL+"WebsiteUserInfo"
    static let BOT_CONFIGURATION = URL+"GetBotConfiguration"
    
    //static let Broucher_URL = "http://event.godapark.com/assets/uploads/brochure/"
    
}	
