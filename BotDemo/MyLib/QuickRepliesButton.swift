//
//  CustomButton.swift
//  DrawableExample
//
//  Created by Swapnil Dhavan on 07/01/19.
//  Copyright © 2019 Test. All rights reserved.
//

import UIKit

class QuickRepliesButton: UIButton {
    
    override func awakeFromNib() {
        super.awakeFromNib()
       /* self.layer.masksToBounds = false
        self.layer.cornerRadius = 10
        self.layer.shadowColor = UIColor.black.cgColor
        self.layer.shadowOffset = CGSize(width: 0, height: 3)
        self.layer.shadowOpacity = 0.5
        self.layer.shadowRadius = 5.0*/
        
        var maskPath = UIBezierPath()
        let maskLayer = CAShapeLayer()
        
        maskPath = UIBezierPath(roundedRect: bounds,
        byRoundingCorners: [.topRight, .bottomLeft, .bottomRight],
        cornerRadii: CGSize(width: frame.height/2, height: frame.height/2))
        maskLayer.path = maskPath.cgPath
        //self.backgroundColor = UIColor(red: 171/255, green: 178/255, blue: 186/255, alpha: 1.0)
        // Shadow and Radius
        self.layer.shadowColor = UIColor(red: 0, green: 0, blue: 0, alpha: 0.25).cgColor
        self.layer.shadowOffset = CGSize(width: 0.0, height: 1.0)
        self.layer.shadowOpacity = 1.0
        self.layer.shadowPath = maskPath.cgPath
        self.layer.mask = maskLayer
        //self.layer.shadowRadius = 4.0
        self.layer.masksToBounds = false
       // self.layer.cornerRadius = frame.height/2
        
        let borderLayer = CAShapeLayer()
        borderLayer.path = maskPath.cgPath
        borderLayer.lineWidth = 2
        borderLayer.strokeColor = Constants.shared.hexStringToUIColor(hex: (Pref.getPref(key: Constants.shared.THEME_COLOR) as! String)).cgColor
        borderLayer.fillColor = UIColor.clear.cgColor
        borderLayer.frame = self.bounds
        self.layer.addSublayer(borderLayer)
        
        //self.layer.borderWidth = 1
        //self.layer.borderColor = UIColor.blue.cgColor
        
    }
}
