//
//  ChatVC.swift
//  DrawableExample
//
//  Created by Swapnil Dhavan on 20/11/18.
//  Copyright © 2018 Test. All rights reserved.
//

import UIKit
import AVKit
import AVFoundation
import AlamofireImage
import Alamofire
import ISEmojiView
import Speech
import FLAnimatedImage

class ChatVC: UIViewController,UITableViewDelegate,UITableViewDataSource, EmojiViewDelegate, SFSpeechRecognizerDelegate {
    
    @IBOutlet weak var tableView: UITableView!
    
    @IBOutlet weak var tfChatText: UITextField!
    
    @IBOutlet weak var btnSend: UIButton!
    @IBOutlet weak var imageSliderView: ImageSlider!
    @IBOutlet weak var btnSmiles: UIButton!
    @IBOutlet weak var bottomViewConstraints: NSLayoutConstraint!
    @IBOutlet weak var btnSpeech: UIButton!
    @IBOutlet weak var viewTopBar: UIView!
    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var imgBotAvtar: UIImageView!
    @IBOutlet weak var mainView: UIView!
    
    var chatArr: Array=[ChatData]()
    var refreshControl = UIRefreshControl()
    var sender_id: String = ""
    var receiver_id: String = ""
    var chat_name: String = ""
    var is_refresh: Bool = true
    var page_count = 1
    var remaining = 0
    var webV = UIWebView()

    //For Emojis
    let keyboardSettings = KeyboardSettings(bottomType: .categories)
    var emojiView: EmojiView!
    
    //For Speech To text
    private let speechRecognizer = SFSpeechRecognizer(locale: Locale.init(identifier: "en-US"))!
    private var recognitionRequest: SFSpeechAudioBufferRecognitionRequest?
    private var recognitionTask: SFSpeechRecognitionTask?
    private let audioEngine = AVAudioEngine()
    
    var innterText: String!
    var outerText: String!
    var themeColor: String!
    var botAvtar: String!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationItem.backBarButtonItem = UIBarButtonItem(title:"", style:.plain, target:nil, action:nil)

        //Constants.menuItem(menuBtn: btnMenuButton, reaveal: revealViewController(), sview: self.view)
        tableView.tableFooterView = UIView()
        //btnSend.contentEdgeInsets = UIEdgeInsets(top: 5, left: 5, bottom: 5, right: 5)
        btnSend.layer.cornerRadius = 10.0
        
        emojiView = EmojiView(keyboardSettings: keyboardSettings)
        emojiView.translatesAutoresizingMaskIntoConstraints = false
        emojiView.delegate = self
        
        tableView.transform = CGAffineTransform(rotationAngle: -CGFloat.pi)
        SpeechToText()
        getBotConfig()
        // Do any additional setup after loading the view.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
            // Hide the Navigation Bar
            self.navigationController?.setNavigationBarHidden(true, animated: true)
        }

    func emojiViewDidSelectEmoji(_ emoji: String, emojiView: EmojiView) {
        tfChatText.insertText(emoji)
    }
    func emojiViewDidPressDeleteBackwardButton(_ emojiView: EmojiView) {
        tfChatText.deleteBackward()
    }
    
    @IBAction func btnChangeKeyboardTap(_ sender: Any) {
        if tfChatText.inputView == nil{
            tfChatText.inputView = emojiView
            tfChatText.becomeFirstResponder()
            tfChatText.reloadInputViews()
            btnSmiles.setImage(UIImage(named: "keyboard"), for: .normal)
            
        }else{
            tfChatText.inputView = nil
            tfChatText.keyboardType = .default
            tfChatText.becomeFirstResponder()
            tfChatText.reloadInputViews()
            btnSmiles.setImage(UIImage(named: "smiling_face"), for: .normal)
        }
    }
    
    @IBAction func btnSendMsg(_ sender: Any) {
        if tfChatText.text != "" {
            //self.chatArr.append(ChatData(id: "1", sender_id: "1", receiver_id: "0", message: tfChatText.text!))
            self.chatArr.insert(ChatData(message: tfChatText.text!,check_sending: 0), at: 0)
            self.tableView.reloadData()
            addChatMsg(message: tfChatText.text!, lat: "", long: "", input_msg_type: Constants.shared.ITM_MESSAGE)
        }
    }
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
        page_count = 1
        remaining = 0
        
        self.title = chat_name
        
        NotificationCenter.default.addObserver(self, selector: #selector(keyboard), name: UIResponder.keyboardWillShowNotification, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(keyboard), name: UIResponder.keyboardWillHideNotification, object: nil)
    
        refreshControl.attributedTitle = NSAttributedString(string: "")
        refreshControl.addTarget(self, action: #selector(refresh), for: UIControl.Event.valueChanged)
        tableView.addSubview(refreshControl) // not required when using UITableViewController
        //timer = Timer.scheduledTimer(timeInterval: 3.0, target: self, selector:#selector(ChatVC.refresh), userInfo: nil, repeats: true
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        // Show the Navigation Bar
        self.navigationController?.setNavigationBarHidden(false, animated: false)
        //timer.invalidate()
        if self.isMovingFromParent {}
    }
    
    @objc func keyboard(notification: NSNotification) {
        
        let keyboardScreenEndFrame = (notification.userInfo![UIResponder.keyboardFrameEndUserInfoKey] as! NSValue).cgRectValue
        if notification.name == UIResponder.keyboardWillHideNotification {
            bottomViewConstraints.constant = 0
           // scrollView.contentInset = UIEdgeInsets.zero
        }else if bottomViewConstraints.constant == 0{
            bottomViewConstraints.constant -= keyboardScreenEndFrame.height
            //bottomViewConstraints.constant -= 260 // emojiview keyboard height
           /* if tfChatText.inputView == emojiView{
            
                bottomViewConstraints.constant -= 216 // emojiview keyboard height
            }else{
                bottomViewConstraints.constant -= keyboardScreenEndFrame.height
                
            }*/
            //scrollView.contentInset = UIEdgeInsets(top: 0, left: 0, bottom: keyboardScreenEndFrame.height,right: 0)
        }
        
        UIView.animate(withDuration: 2, delay: 0, options: .curveEaseOut, animations: {
             self.view.layoutIfNeeded()
        }, completion: {(completed) in})
        //scrollView.scrollIndicatorInsets = scrollView.contentInset
    }
    
   /* @objc func keyboardWillShow(notification: NSNotification) {
        
        if let keyboardSize = (notification.userInfo?[UIResponder.keyboardFrameBeginUserInfoKey] as? NSValue)?.cgRectValue {
            if self.view.frame.origin.y == 0{
                self.view.frame.origin.y -= keyboardSize.height
            }
        }
    }
    
    @objc func keyboardWillHide(notification: NSNotification) {
        if let keyboardSize = (notification.userInfo?[UIResponder.keyboardFrameBeginUserInfoKey] as? NSValue)?.cgRectValue {
            if self.view.frame.origin.y != 0{
                self.view.frame.origin.y += keyboardSize.height
            }
        }
    }*/
    
    @objc func refresh(){
        page_count = 1
        remaining = 0
        if is_refresh{
            //chatArr.removeAll()
            //tableView.reloadData()
           // getChatList()
        }
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return chatArr.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "ChatTVCell") as! ChatTVCell
        cell.viewController = self
        //cell.quickReplyArr = chatArr[indexPath.row].quickReplyArr
        //cell.carouselArr = chatArr[indexPath.row].carouselArr
        //cell.cvQuickReplies.reloadData()
        //cell.cvQuickReplies.layoutIfNeeded()
        //cell.collectionView.reloadData()
        cell.senderView.backgroundColor = Constants.shared.hexStringToUIColor(hex: self.themeColor)
        cell.viewLoading.tintColor = Constants.shared.hexStringToUIColor(hex: self.themeColor)
        
        cell.imgBotImage.image = self.imgBotAvtar.image
        cell.imgBotImage2.image = self.imgBotAvtar.image
        
        cell.quickReplyArr = [QuickReply]()
        cell.cvQuickReplies.reloadData()
        cell.cvQuickReplies.layoutIfNeeded()
        
        //cell.imageSliderView.delegate = self
        cell.themeColor = themeColor
        cell.botAvtar = botAvtar
        cell.botImage = self.imgBotAvtar.image
        
        cell.lblReceiverMsg.text! = ""
        //cell.lblReceiverDatetime.text! = ""
        cell.lblSenderMsg.text! = ""
        cell.lblSenderDatetime.text! = ""
        cell.receiverView.isHidden = true
        cell.senderView.isHidden = true
        cell.imgBotImage.isHidden = true
        cell.imgBotImage2.isHidden = true
        cell.btnCarouselRightArrow.isHidden = true
        cell.btnCarouselLeftArrow.isHidden = true

        cell.viewVideoHeightConsraints.constant = 0
        cell.imgHeightConstraints.constant = 0
        cell.cardViewDotsHeight.constant = 0
       // cell.viewQuickRplHeight.constant = 0
        cell.viewCarouselHeightConstatints.constant = 0
        cell.buttonsHeight.constant = 0
        cell.btnPlayVideo.isHidden = true
        
        if chatArr[indexPath.row].message_type == Constants.shared.SENDER {
             
            if chatArr[indexPath.row].created_at ==  "sending..."{
                cell.cardViewDotsHeight.constant = 30
                cell.imgBotImage2.isHidden = false
            }else{
                cell.imgBotImage2.isHidden = true
                cell.cardViewDotsHeight.constant = 0
            }
            
            cell.lblReceiverDatetime.text! = ""
            cell.lblSenderMsg.text! = chatArr[indexPath.row].message
            cell.lblSenderDatetime.text! = chatArr[indexPath.row].created_at
            cell.senderView.isHidden = false
                
        }else{
            cell.cardViewDotsHeight.constant = 0
            cell.lblReceiverDatetime.text! = chatArr[indexPath.row].created_at
            if chatArr[indexPath.row].template_type == Constants.shared.TEXT_MESSAGE{
                cell.lblReceiverMsg.text! = chatArr[indexPath.row].message
                cell.receiverView.isHidden = false
                cell.imgBotImage.isHidden = false
                
                if chatArr[indexPath.row].quickReplyArr.count > 0{
                    cell.configure(with: chatArr[indexPath.row].quickReplyArr)
                    //cell.viewQuickRplHeight.constant = 50
                   // cell.viewQuickRplHeight.constant = CGFloat((55 * chatArr[indexPath.row].quickReplyArr.count))
                }else{
                    //cell.viewQuickRplHeight.constant = 0
                }
            }else
                if chatArr[indexPath.row].template_type == Constants.shared.VIDEO
                {
                    cell.imgBotImage2.isHidden = false
                    cell.btnPlayVideo.isHidden = false
                    cell.videoURL = chatArr[indexPath.row].url
                    cell.viewVideoHeightConsraints.constant = 150
                    
                    //for getting thumbnail
                    AVAsset(url: URL(string: chatArr[indexPath.row].url)!).generateThumbnail { [weak self] (image) in
                        DispatchQueue.main.async {
                            guard let image = image else { return }
                            cell.imgVideoThumbnail.image = image
                        }
                    }
                  
                }else if chatArr[indexPath.row].template_type == Constants.shared.IMAGE
                {
                    cell.imgBotImage2.isHidden = false
                    //cell.imgView.layer.cornerRadius = 10.0
                    
                    //cell.imgView.image = UIImage.gif(url: "http://www.gifbin.com/bin/4802swswsw04.gif")
                    //cell.imgView.image = UIImage.gif(url: chatArr[indexPath.row].url)
                    //let url = URL(string: "http://giphygifs.s3.amazonaws.com/media/Pm45yRYElIcyA/giphy.gif")!
                    //let imageData = try? Data(contentsOf: url)
                    //let imageData3 = FLAnimatedImage(animatedGIFData: imageData)
                    //cell.imgView.animatedImage = imageData3
                    Constants.shared.loadImg(imgName: chatArr[indexPath.row].url, imageView: cell.imgView, DefaultImg: UIImage(named: "bot")!)
                    cell.imgHeightConstraints.constant = 150
                }else if chatArr[indexPath.row].template_type == Constants.shared.CAROUSEL
                {
                    
                    
                    //cell.btnCarouselLeftArrow.isHidden = false
                    if chatArr[indexPath.row].carouselArr.count > 1{
                        cell.btnCarouselRightArrow.isHidden = false
                    }

                    cell.imgBotImage2.isHidden = false
                    cell.carouselArr = chatArr[indexPath.row].carouselArr
                    
                    //cell.viewCarouselHeightConstatints.constant = CGFloat((chatArr[indexPath.row].buttonArr.count * 40)+180)
                    //cell.viewCarouselHeightConstatints.constant = CGFloat(260)
                    cell.viewCarouselHeightConstatints.constant = CGFloat((chatArr[indexPath.row].buttonCount * 40)+175+chatArr[indexPath.row].titleHeight+chatArr[indexPath.row].titleHeight)
                    
                    cell.crTitleHeight = chatArr[indexPath.row].titleHeight
                    cell.crSubTitleHeight = chatArr[indexPath.row].subTitleHeight
                
                    cell.collectionView.reloadData()
                    cell.collectionView.layoutIfNeeded()
                    
            }else if chatArr[indexPath.row].template_type == Constants.shared.BUTTONS
                {
                    cell.imgBotImage2.isHidden = false
                    if chatArr[indexPath.row].buttonArr.count > 0{
                      cell.buttonsHeight.constant = CGFloat((55 * chatArr[indexPath.row].buttonArr.count)+100)
                    }
                    cell.lblButtonTitle.text! = chatArr[indexPath.row].message
                    cell.buttonArr = chatArr[indexPath.row].buttonArr
                    cell.cvButtons.reloadData()
            }
        }

        cell.collectionView.reloadData()
        cell.transform = CGAffineTransform(rotationAngle: CGFloat.pi)
        cell.selectionStyle = UITableViewCell.SelectionStyle.none
        return cell
    }
    
    /*func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
     let mainStoryBoard:UIStoryboard = UIStoryboard(name: "Main",bundle: nil)
     let desController = mainStoryBoard.instantiateViewController(withIdentifier: "SpeakerDetailsVC") as! SpeakerDetailsVC
     desController.SpeakerArr = [speakerArr[indexPath.row]]
     self.navigationController?.pushViewController(desController, animated: true)
     }*/
    
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        
      //  if indexPath.row == chatArr.count - 1 && remaining > 0 {
            page_count = page_count + 1
            //self.indicator.startAnimating()
            //LoadingOverlay.shared.showOverlay(view: self.view)
            //getChatList()
      //  }
    }
    
    func SpeechToText(){
        btnSpeech.isEnabled = false
        
        speechRecognizer.delegate = self
        
        SFSpeechRecognizer.requestAuthorization { (authStatus) in
            
            var isButtonEnabled = false
            
            switch authStatus {
            case .authorized:
                isButtonEnabled = true
                print("Enabled")
            case .denied:
                isButtonEnabled = false
                print("User denied access to speech recognition")
                
            case .restricted:
                isButtonEnabled = false
                print("Speech recognition restricted on this device")
                
            case .notDetermined:
                isButtonEnabled = false
                print("Speech recognition not yet authorized")
            @unknown default:
                print("Default Error")
            }
            
            OperationQueue.main.addOperation() {
                print("Enabled12")
                self.btnSpeech.isEnabled = isButtonEnabled
            }
        }
    }
    
    @IBAction func btnSpeechTap(_ sender: Any) {
        if audioEngine.isRunning {
            audioEngine.stop()
            recognitionRequest?.endAudio()
            btnSpeech.isEnabled = false
            //btnSpeech.setTitle("Start Recording", for: .normal)
            btnSpeech.setImage(UIImage(named: "microphone"), for: .normal)
        } else {
            startRecording()
            //btnSpeech.setTitle("Stop Recording", for: .normal)
            btnSpeech.setImage(UIImage(named: "stop"), for: .normal)
        }
    }
    
    func startRecording() {
        
        if recognitionTask != nil {  //1
            recognitionTask?.cancel()
            recognitionTask = nil
        }
        
        let audioSession = AVAudioSession.sharedInstance()  //2
        do {
            try audioSession.setCategory(AVAudioSession.Category.record)
            try audioSession.setMode(AVAudioSession.Mode.measurement)
            try audioSession.setActive(true, options: .notifyOthersOnDeactivation)
        } catch {
            print("audioSession properties weren't set because of an error.")
        }
        
        recognitionRequest = SFSpeechAudioBufferRecognitionRequest()  //3
        
       let inputNode = audioEngine.inputNode
     //  fatalError("Audio engine has no input node")
       
        
        guard let recognitionRequest = recognitionRequest else {
            fatalError("Unable to create an SFSpeechAudioBufferRecognitionRequest object")
        } //5
        
        recognitionRequest.shouldReportPartialResults = true  //6
        
        recognitionTask = speechRecognizer.recognitionTask(with: recognitionRequest, resultHandler: { (result, error) in  //7
            
            var isFinal = false  //8
            
            if result != nil {
                
                print("result: ",result?.bestTranscription.formattedString)
                
                //self.tfChatText.text = result?.bestTranscription.formattedString  //9
              /*  self.chatArr.insert(ChatData(id: "1", sender_id: "1", receiver_id: "0", message: (result?.bestTranscription.formattedString)!), at: 0)
                self.tableView.reloadData()
                self.addChatMsg()
                self.audioEngine.stop()
                recognitionRequest.endAudio()
                self.btnSpeech.isEnabled = false
                //btnSpeech.setTitle("Start Recording", for: .normal)
                self.btnSpeech.setImage(UIImage(named: "microphone"), for: .normal)*/
               
                isFinal = (result?.isFinal)!
            }
            
            if error != nil || isFinal {  //10
                
                print("result123: ",result?.bestTranscription.formattedString)
                if result != nil {
                self.chatArr.insert(ChatData(message: (result?.bestTranscription.formattedString)!, check_sending: 0), at: 0)
                self.tableView.reloadData()
                    self.addChatMsg(message: (result?.bestTranscription.formattedString)!, lat: "", long: "", input_msg_type: Constants.shared.ITM_MESSAGE)
                self.audioEngine.stop()
                recognitionRequest.endAudio()
                self.btnSpeech.isEnabled = false
                //btnSpeech.setTitle("Start Recording", for: .normal)
                    self.btnSpeech.setImage(UIImage(named: "microphone"), for: .normal)}
                
                self.audioEngine.stop()
                inputNode.removeTap(onBus: 0)
                
                self.recognitionRequest = nil
                self.recognitionTask = nil
                
                self.btnSpeech.isEnabled = true
            }
        })
        
        let recordingFormat = inputNode.outputFormat(forBus: 0)  //11
        inputNode.installTap(onBus: 0, bufferSize: 1024, format: recordingFormat) { (buffer, when) in
            self.recognitionRequest?.append(buffer)
        }
        audioEngine.prepare()  //12
        do {
            try audioEngine.start()
        } catch {
            print("audioEngine couldn't start because of an error.")
        }
        
       // textView.text = "Say something, I'm listening!"
        
    }
    
    func speechRecognizer(_ speechRecognizer: SFSpeechRecognizer, availabilityDidChange available: Bool) {
        if available {
            btnSpeech.isEnabled = true
        } else {
            btnSpeech.isEnabled = false
        }
    }
    
   func getBotConfig(){
    
        ProgressLoading.showLoading()
    
        var parameters = [String:Any]()
        parameters["WebsiteId"] = "27beb9c9-10b0-ec66-5371-91acbd65110c"
        
        Alamofire.request(MyConfig.BOT_CONFIGURATION, method: .get, parameters: parameters, encoding: URLEncoding.default, headers: nil).responseJSON { response in
            
            let json = JSON(response.result.value as Any)
            print(json)
            //let status: Bool = json["status"].bool!
            //let message: String = json["message"].stringValue
            //self.remaining = json["remaining"].intValue
            //self.is_refresh = status
            ProgressLoading.hide()
            do{
                let result = JSON(json)
                self.innterText = result["BotInnerText"].stringValue
                self.outerText = result["BotOuterText"].stringValue
                self.themeColor = result["BotColor"].stringValue
                self.botAvtar = result["BotAvatar"].stringValue
                
                Pref.setPref(key: Constants.shared.THEME_COLOR, value: result["BotColor"].stringValue)
            
                self.lblTitle.text! = result["BotInnerText"].stringValue
                self.viewTopBar.backgroundColor = Constants.shared.hexStringToUIColor(hex: self.themeColor)
                Constants.shared.loadImg(imgName: self.botAvtar,imageView: self.imgBotAvtar, DefaultImg: UIImage(named: "bot_avtar")!)
                
                UINavigationBar.appearance().barTintColor = Constants.shared.hexStringToUIColor(hex: self.themeColor)
                UINavigationBar.appearance().tintColor = UIColor.white
                UINavigationBar.appearance().titleTextAttributes = [NSAttributedString.Key.foregroundColor:UIColor.white]
                UINavigationBar.appearance().isTranslucent = false
                
                //self.refreshControl.endRefreshing()
            }catch{
                print(error.localizedDescription)
            }
        }
    }
    
    func addChatMsg(message: String,lat: String, long: String,input_msg_type: String){
    
        //self.refreshControl.beginRefreshing()
        var parameters = [String:Any]()
        //parameters["event_id"] = Pref.getPref(key: Constants.EVENT_ID) as? String
        
        let dateTime = String(Date().millisecondsSince1970)
        let message_id = randomString(length: 40)
        
        var json = ""
        if input_msg_type == Constants.shared.ITM_LOCATION{
            json =  "{\"messaging\":[{\"sender\":{\"id\":\"6a575c17-9b2c-4ce7-83e6-0c3b32b6adb3\"},\"recipient\":{\"id\":\"27beb9c9-10b0-ec66-5371-91acbd65110c\"},\"timestamp\":\""+dateTime+"\",\"message\":{\"mid\":\""+message_id+"\",\"seq\":\"0\",\"attachments\":[{\"title\":\"Andheri\",\"url\":\"\",\"type\":\"location\",\"payload\":{\"coordinates\":{\"lat\":\""+lat+"\",\"long\":\""+long+"\"}}}]}}]}"
        }else if input_msg_type == Constants.shared.ITM_QUICKRPL{
            json = "{\"messaging\":[{\"sender\":{\"id\":\"e71e9e37-ac5e-4ffe-b411-6c81fc1024bb\"},\"recipient\":{\"id\":\"27beb9c9-10b0-ec66-5371-91acbd65110c\"},\"timestamp\":\""+dateTime+"\",\"message\":{\"mid\":\""+message_id+"\",\"text\":\""+message+"\",\"quick_reply\":{\"payload\":\""+message+"\"}}}]}"
        }else if input_msg_type == Constants.shared.ITM_BUTTON{
            json = "{\"messaging\":[{\"sender\":{\"id\":\"e71e9e37-ac5e-4ffe-b411-6c81fc1024bb\"},\"recipient\":{\"id\":\"27beb9c9-10b0-ec66-5371-91acbd65110c\"},\"timestamp\":\""+dateTime+"\",\"postback\":{\"title\":\""+message+"\",\"payload\":\""+message+"\"}}]}"
        }else if input_msg_type == Constants.shared.ITM_MESSAGE{
            json = "{\"messaging\":[{\"sender\":{\"id\":\"6a575c17-9b2c-4ce7-83e6-0c3b32b6adb3\"},\"recipient\":{\"id\":\"27beb9c9-10b0-ec66-5371-91acbd65110c\"},\"timestamp\":\""+dateTime+"\",\"message\":{\"mid\":\""+message_id+"\",\"text\":\""+message+"\"}}]}"
        }
        
        parameters["reference"] = "aditya.com"
        parameters["msg"] = JSON(json)
        //parameters["abc"] = JSON(json1)
        
        print("Parameters: ", parameters);
        tfChatText.text = ""
        
        let url = MyConfig.SEND_MESSAGE+"?msg="+JSON(json).string!+"&reference=aditya.com"
        
        Alamofire.request(MyConfig.SEND_MESSAGE
            , method: .get, parameters: parameters, encoding: URLEncoding.default, headers: nil).responseJSON { response in
            
                self.chatArr[0].created_at = "delivered."
                
            //LoadingOverlay.shared.hideOverlayView()
            print("Result: ",response.result.value)
            
            let result = JSON(response.result.value as Any)
            let message = JSON(result)
            print("Json: ",message)
            
                /*********** send message date response like sending and deliveed *********/
                let dateFormatter = DateFormatter()
                //dateFormatter.dateFormat = "yyyy-MM-dd HH:mm:ss"
                let created_at: Date = Date()
                //dateFormatter.dateFormat = "dd MMM yyyy hh:mm aa"
                dateFormatter.dateFormat = "hh:mm aa"
                self.chatArr[0].created_at = dateFormatter.string(from: created_at)
                /***********  End  *********/
                
                for arr in result.arrayValue{
                    self.chatArr.insert(ChatData(json: arr), at: 0)
                }
                
            self.refreshControl.endRefreshing()
            self.tableView.reloadData()
        }
    }
    
    /*func SendLocation(lat: String, long: String){
         let url = URL(string: MyConfig.SEND_MESSAGE)!
         var request = URLRequest(url: url)
         request.setValue("application/x-www-form-urlencoded", forHTTPHeaderField: "Content-Type")
         request.httpMethod = "POST"
         var parameters = [String:Any]()
          //parameters["event_id"] = Pref.getPref(key: Constants.EVENT_ID) as? String
          
        let json =  "{\"messaging\":[{\"sender\":{\"id\":\"6a575c17-9b2c-4ce7-83e6-0c3b32b6adb3\"},\"recipient\":{\"id\":\"27beb9c9-10b0-ec66-5371-91acbd65110c\"},\"timestamp\":\"1568375391970\",\"message\":{\"mid\":\"123445446\",\"seq\":\"0\",\"attachments\":[{\"title\":\"Andheri\",\"url\":\"\",\"type\":\"location\",\"payload\":{\"coordinates\":{\"lat\":\""+lat+"\",\"long\":\""+long+"\"}}}]}}]}"
         /* }else{
              json = "{\"messaging\":[{\"sender\":{\"id\":\"6a575c17-9b2c-4ce7-83e6-0c3b32b6adb3\"},\"recipient\":{\"id\":\"27beb9c9-10b0-ec66-5371-91acbd65110c\"},\"timestamp\":1568375391970,\"message\":{\"mid\":\"d6fc0ee3-90f8-43a5-8909-6a18ffd2cdbb\",\"text\":\""+message+"\"}}]}"
          }*/
          parameters["reference"] = "aditya.com"
          parameters["msg"] = JSON(json)
          //parameters["abc"] = JSON(json1)
          
          print("Parameters: ", parameters);
         request.httpBody = parameters.percentEscaped().data(using: .utf8)

         let task = URLSession.shared.dataTask(with: request) { data, response, error in
             guard let data = data,
                 let response = response as? HTTPURLResponse,
                 error == nil else {                                              // check for fundamental networking error
                 print("error", error ?? "Unknown error")
                 return
             }

             guard (200 ... 299) ~= response.statusCode else {                    // check for http errors
                 print("statusCode should be 2xx, but is \(response.statusCode)")
                 print("response = \(response)")
                 return
             }

             let responseString = String(data: data, encoding: .utf8)
             let result = JSON(responseString)
             let message = JSON(result)
             print("Json: ",result)
             print("Message: ",JSON(message["message"]))
             let text = JSON(message["message"])
                 
                 /*********** send message date response like sending and deliveed *********/
                 let dateFormatter = DateFormatter()
                 //dateFormatter.dateFormat = "yyyy-MM-dd HH:mm:ss"
                 let created_at: Date = Date()
                 dateFormatter.dateFormat = "dd MMM yyyy hh:mm aa"
                 self.chatArr[0].created_at = dateFormatter.string(from: created_at)
                 /***********  End *********/
                 
                 for arr in result.arrayValue{
                     self.chatArr.insert(ChatData(json: arr), at: 0)
                 }
                 
            // self.refreshControl.endRefreshing()
             
             print("responseString = \(responseString)")
         }

         task.resume()
         self.tableView.reloadData()
    }*/
    
    @objc func dismissKeyboard() {
        view.endEditing(true)
        // do aditional stuff
    }
    
    func randomString(length: Int) -> String {
        let letters = "-abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789"+"aditya.com\(String(Date().millisecondsSince1970))"
        return String((0..<length).map{ _ in letters.randomElement()! })
    }
}

class ChatTVCell: UITableViewCell, UICollectionViewDataSource, UICollectionViewDelegate, UICollectionViewDelegateFlowLayout{
    
    @IBOutlet weak var lblReceiverMsg: UILabel!
    //@IBOutlet weak var btnLike: UIButton!
    @IBOutlet weak var lblReceiverDatetime: UILabel!
    
    @IBOutlet weak var lblSenderMsg: UILabel!
    @IBOutlet weak var lblSenderDatetime: UILabel!
    @IBOutlet weak var senderView: CardView!
    @IBOutlet weak var receiverView: CardView!
    @IBOutlet weak var imgVideoThumbnail: UIImageView!
    @IBOutlet weak var imageSliderView: ImageSlider!
    @IBOutlet weak var collectionView: UICollectionView!
    @IBOutlet weak var cvQuickReplies: UICollectionView!
    @IBOutlet weak var cvButtons: UICollectionView!
    @IBOutlet weak var viewLoading: DotsLoader!
    
    @IBOutlet weak var imgView: FLAnimatedImageView!
    @IBOutlet weak var imgBotImage: UIImageView!
    @IBOutlet weak var imgBotImage2: UIImageView!
    
    @IBOutlet weak var viewVideoHeightConsraints: NSLayoutConstraint!
    @IBOutlet weak var btnPlayVideo: UIButton!
    
    @IBOutlet weak var imgHeightConstraints: NSLayoutConstraint!
    
    @IBOutlet weak var viewCarouselHeightConstatints: NSLayoutConstraint!
    @IBOutlet weak var cardViewDotsHeight: NSLayoutConstraint!
    @IBOutlet weak var viewQuickRplHeight: NSLayoutConstraint!
    @IBOutlet weak var buttonsHeight: NSLayoutConstraint!

    @IBOutlet weak var lblButtonTitle: UILabel!
    
    @IBOutlet weak var btnCarouselRightArrow: UIButton!
    @IBOutlet weak var btnCarouselLeftArrow: UIButton!
    
    //for increasing title and subtitle Height
    var crTitleHeight = 0
    var crSubTitleHeight = 0
    
    var count: Int = 0
    var viewController: ChatVC!
    //let videoURL = URL(string: "https://clips.vorwaerts-gmbh.de/big_buck_bunny.mp4")
    var videoURL: String = ""
    var imgURL: String = ""
    var themeColor: String!
    var botAvtar: String!
    var botImage: UIImage!
    var quickReplyArr: Array = [QuickReply]()
    var carouselArr: Array = [Caraousel]()
    var buttonArr: Array=[Buttons]()
    
    func configure(with quickReplyArr: [QuickReply]) {
        self.quickReplyArr = quickReplyArr
        self.cvQuickReplies.reloadData()
        self.cvQuickReplies.layoutIfNeeded()
    }
    
    override func awakeFromNib() {
        //super.awakeFromNib()
        //senderView.backgroundColor = Constants.shared.hexStringToUIColor(hex: themeColor)
        //self.lblTitle.text! = result["BotInnerText"].stringValue
        //imgBotImage.image = botImage
        //Constants.shared.loadImg(imgName: self.botAvtar,imageView: self.imgBotImage, DefaultImg: UIImage(named: "bot_avtar")!)
        
        self.collectionView.delegate = self
        self.collectionView.dataSource = self
        self.cvQuickReplies.delegate = self
        self.cvQuickReplies.dataSource = self
        self.cvButtons.delegate = self
        self.cvButtons.dataSource = self
        self.cvQuickReplies.reloadData()
        self.collectionView.reloadData()
        self.cvButtons.reloadData()
        
        //let layout = collectionView.collectionViewLayout as! UICollectionViewFlowLayout
       // let width = (collectionView.frame.size.width)
        //layout.itemSize = CGSize(width: width, height: 300)
        
        //let layout = cvQuickReplies.collectionViewLayout as! UICollectionViewFlowLayout
        //let width = 80
        //layout.itemSize = CGSize(width: width, height: 50)
       
        //lblSenderMsg.contentEdgeInsets = UIEdgeInsets(top: 5, left: 5, bottom: 5, right: 5)
        lblSenderMsg.layer.cornerRadius = 10.0
        lblReceiverMsg.layer.cornerRadius = 10.0
        imgVideoThumbnail.layer.cornerRadius = 30.0
        imgView.layer.cornerRadius = 30.0
        btnPlayVideo.layer.cornerRadius = 30.0
        collectionView.layer.cornerRadius = 30.0
    
    }
    
    @IBAction func btnPlayVideoTap(_ sender: Any) {
        
        let player = AVPlayer(url: URL(string: videoURL)!)
        let playerViewController = AVPlayerViewController()
        playerViewController.player = player
        viewController.present(playerViewController, animated: true) {
            playerViewController.player!.play()
        }
    }
    
    @IBAction func btnLeftCarouselTap(_ sender: Any) {
        if count > 0 {
        count -= 1
        collectionView.scrollToItem(at: IndexPath(row: count, section: 0), at: .right, animated: true)
        }
        if count == 0 {
            btnCarouselLeftArrow.isHidden = true
        }else{
            btnCarouselRightArrow.isHidden = false
        }
    }
    @IBAction func btnRightCarouselTap(_ sender: Any) {
        if count < (carouselArr.count-1){
            count += 1
        collectionView.scrollToItem(at: IndexPath(row: count, section: 0), at: .right, animated: true)
        }
        if count == (carouselArr.count-1) {
            btnCarouselRightArrow.isHidden = true
        }else{
            btnCarouselLeftArrow.isHidden = false
        }
    }
    
    func scrollViewDidEndDecelerating(_ scrollView: UIScrollView) {
        for cell in collectionView.visibleCells {
            let indexPath = collectionView.indexPath(for: cell)
            count = indexPath!.row
        }
    }
    
    
    // MARK: UICollectionViewDataSource
    func numberOfSectionsInCollectionView(collectionView: UICollectionView) -> Int {
        return 1
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        
        var count = 0
        if collectionView == cvQuickReplies{
            count = quickReplyArr.count
        }else if collectionView == self.collectionView{
            count = carouselArr.count
        }else if collectionView == cvButtons{
            count = buttonArr.count
        }
        return count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        if collectionView == cvQuickReplies {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "QuickReplyCVCell", for: indexPath) as! QuickReplyCVCell
        cell.viewController = viewController
            /*if quickReplyArr[indexPath.row].title.isEmpty{
                cell.lblQuickRplTitle.text! = "Pick Location"
                //cell.btnQuickRplTitle.setTitle("Pick Location", for: .normal)
            }else{
                cell.lblQuickRplTitle.text! = quickReplyArr[indexPath.row].title
                //cell.btnQuickRplTitle.setTitle(quickReplyArr[indexPath.row].title, for: .normal)
            }*/
            cell.lblQuickRplTitle.text! = quickReplyArr[indexPath.row].title

        cell.payload = quickReplyArr[indexPath.row].payload
        cell.content_type = quickReplyArr[indexPath.row].content_type
            cell.layoutIfNeeded()
        return cell
        }else if collectionView == self.cvButtons{
            
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "ButtonsCVCell", for: indexPath) as! ButtonsCVCell
            cell.viewController = viewController
            cell.btnButtonTitle.setTitle(buttonArr[indexPath.row].title, for: .normal)
            cell.url = buttonArr[indexPath.row].url
            cell.payload = buttonArr[indexPath.row].payload
            cell.content_type = buttonArr[indexPath.row].type
            cell.layoutIfNeeded()
        return cell
        }
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "CarouselCVCell", for: indexPath) as! CarouselCVCell
        cell.btnOne.setTitle("", for: .normal)
        cell.btnTwo.setTitle("", for: .normal)
        cell.btnThree.setTitle("", for: .normal)
        cell.carouselButtonOneHeight.constant = 0
        cell.carouselButtonTwoHeight.constant = 0
        cell.carouselButtonThreeHeight.constant = 0
        //viewCarouselHeightConstatints.constant = 0
        Constants.shared.loadImg(imgName: carouselArr[indexPath.row].image_url,imageView: cell.imgBack!, DefaultImg: UIImage(named: "bot_avtar")!)
           //cell.imgBack.image = UIImage.gif(url: carouselArr[indexPath.row].image_url)
           cell.lblTitle.text! = carouselArr[indexPath.row].title
           cell.lblSubTitle.text! = carouselArr[indexPath.row].subtitle
           cell.lblTitle.baselineAdjustment = .alignCenters
           cell.lblSubTitle.baselineAdjustment = .alignCenters
           cell.viewController = viewController
        
        //viewCarouselHeightConstatints.constant = CGFloat((carouselArr[indexPath.row].buttonsArr.count * 40)+180)
          print("carouselCOunt123: ",carouselArr[indexPath.row].buttonsArr.count)
        
           if (carouselArr[indexPath.row].buttonsArr.count == 1) || (carouselArr[indexPath.row].buttonsArr.count == 2) || (carouselArr[indexPath.row].buttonsArr.count == 3){
           cell.btnOne.setTitle(carouselArr[indexPath.row].buttonsArr[0].title, for: .normal)
           cell.carouselButtonOneHeight.constant = 40
        }
        if (carouselArr[indexPath.row].buttonsArr.count == 2) || (carouselArr[indexPath.row].buttonsArr.count == 3){
            cell.btnTwo.setTitle(carouselArr[indexPath.row].buttonsArr[1].title, for: .normal)
            cell.ButtonArr = carouselArr[indexPath.row].buttonsArr
            cell.carouselButtonTwoHeight.constant = 40
        }
        if (carouselArr[indexPath.row].buttonsArr.count == 3){
            cell.btnTwo.setTitle(carouselArr[indexPath.row].buttonsArr[2].title, for: .normal)
            cell.carouselButtonThreeHeight.constant = 40
        }
        
           //cell.layoutIfNeeded()
        
        return cell
    }
    
    /*public func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        let width = 80
        return CGSize(width: width, height: 50)
    }*/
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        if collectionView == cvQuickReplies{
        let text = self.quickReplyArr[indexPath.row].title
        let cellWidth = text.size(withAttributes:[.font: UIFont.systemFont(ofSize:15.0)]).width + 30.0
            return CGSize(width: cellWidth, height: 40.0)
        }else if collectionView == cvButtons{
            return CGSize(width: 240, height: (55 * buttonArr.count)+100)
        }
        print("carouselHeight123: ",(carouselArr[indexPath.row].buttonsArr.count * 40)+175+crTitleHeight+crSubTitleHeight)
        return CGSize(width: 240, height: (carouselArr[indexPath.row].buttonsArr.count * 40)+175+crTitleHeight+crSubTitleHeight)
    }
    
   func collectionView(_ collectionView: UICollectionView, willDisplay cell: UICollectionViewCell, forItemAt indexPath: IndexPath) {
        if collectionView == self.collectionView{
            count = indexPath.row
            print("count: ",count)
            if count == 0 || carouselArr.count == 1{
                btnCarouselLeftArrow.isHidden = true
            }
            else if count == (carouselArr.count-1) || carouselArr.count == 1{
                btnCarouselRightArrow.isHidden = true
            }
            else if carouselArr.count != 0{
                btnCarouselLeftArrow.isHidden = false
                btnCarouselRightArrow.isHidden = false
            }
        }
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        // Configure the view for the selected state
    }
}

class QuickReplyCVCell: UICollectionViewCell {
    
    var viewController: ChatVC!
    //@IBOutlet weak var btnQuickRplTitle: CustomButton!
    @IBOutlet weak var lblQuickRplTitle: UILabel!
    
    var payload: String = ""
    var content_type: String = ""
    
    override func awakeFromNib() {
        //self.lblQuickRplTitle.text = nil
        
        self.layer.cornerRadius = 10
        self.layer.borderWidth = 1.0
        self.layer.borderColor = Constants.shared.hexStringToUIColor(hex: (Pref.getPref(key: Constants.shared.THEME_COLOR) as! String)).cgColor
        lblQuickRplTitle.textColor = Constants.shared.hexStringToUIColor(hex: (Pref.getPref(key: Constants.shared.THEME_COLOR) as! String))
      /*  var maskPath = UIBezierPath()
        let maskLayer = CAShapeLayer()
         
         maskPath = UIBezierPath(roundedRect: bounds,
         byRoundingCorners: [.topRight, .bottomLeft, .bottomRight],
         cornerRadii: CGSize(width: frame.height/2, height: frame.height/2))
         maskLayer.path = maskPath.cgPath
         //self.backgroundColor = UIColor(red: 171/255, green: 178/255, blue: 186/255, alpha: 1.0)
         // Shadow and Radius
        //self.layer.shadowColor = UIColor(rgb: 0x0069ff).cgColor
         //self.layer.shadowOffset = CGSize(width: 0.0, height: 1.0)
         //self.layer.shadowOpacity = 1.0
         //self.layer.shadowPath = maskPath.cgPath
        //self.layer.borderWidth = 1.0
        
        self.layer.borderColor = UIColor(rgb: 0x0069ff).cgColor
        self.layer.mask = maskLayer
         //self.layer.shadowRadius = 4.0
         self.layer.masksToBounds = true
        // self.layer.cornerRadius = frame.height/2*/
        
        /*let borderLayer = CAShapeLayer()
        borderLayer.path = maskPath.cgPath
        borderLayer.lineWidth = 2
        borderLayer.strokeColor = UIColor(rgb: 0x0069ff).cgColor
        borderLayer.fillColor = UIColor.clear.cgColor
        borderLayer.frame = self.bounds
        self.layer.addSublayer(borderLayer)*/
       
    }
    @IBAction func btnQuickRepliesTap(_ sender: UIButton) {
        if content_type == Constants.shared.LOCATION{
            let storyboard = UIStoryboard(name: "Main", bundle: nil)
            let myAlert = storyboard.instantiateViewController(withIdentifier: "PickCurrentLocationVC") as! PickCurrentLocationVC
            myAlert.modalPresentationStyle = .overCurrentContext
            myAlert.modalTransitionStyle = .crossDissolve
            myAlert.viewController = viewController
            viewController.present(myAlert, animated: true, completion: nil)
        }else{
       // if let buttonTitle = sender.title(for: .normal) {
         viewController.chatArr[0].quickReplyArr.removeAll()
         viewController.chatArr.insert(ChatData(message: lblQuickRplTitle.text!, check_sending: 0), at: 0)
         viewController.tableView.reloadData()
         viewController.addChatMsg(message: payload, lat: "", long: "", input_msg_type: Constants.shared.ITM_QUICKRPL)
       // }
        }
    }
}

class CarouselCVCell: UICollectionViewCell {
    
    var viewController: ChatVC!
    var payload: String = ""
    var url: String = ""
    var type: String = ""
    var ButtonArr: Array = [Buttons]()

    //Crousel Button height
    @IBOutlet weak var carouselButtonOneHeight: NSLayoutConstraint!
    @IBOutlet weak var carouselButtonTwoHeight: NSLayoutConstraint!
    @IBOutlet weak var carouselButtonThreeHeight: NSLayoutConstraint!
    
    @IBOutlet weak var imgBack: UIImageView!
    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var lblSubTitle: UILabel!
    @IBOutlet weak var btnOne: UIButton!
    @IBOutlet weak var btnTwo: UIButton!
    @IBOutlet weak var btnThree: UIButton!
    
    override func awakeFromNib() {
        //backView.layer.cornerRadius = backView.bounds.height/2
      //pulseAnimation.startAnimating()
        //getSegmentTime()
    }
    @IBAction func btnOneTap(_ sender: UIButton) {
        type = ButtonArr[0].type
        payload = ButtonArr[0].payload
        url = ButtonArr[0].url
        btnTap(sender: sender)
    }
    
    @IBAction func btnTwoTap(_ sender: UIButton) {
        type = ButtonArr[1].type
        payload = ButtonArr[1].payload
        url = ButtonArr[1].url
        btnTap(sender: sender)
    }
    @IBAction func btnThreeTap(_ sender: UIButton) {
        type = ButtonArr[2].type
        payload = ButtonArr[2].payload
        url = ButtonArr[2].url
        btnTap(sender: sender)
    }
    
    func btnTap(sender: UIButton){
        if type == Constants.shared.BTN_POSTBACK{
            if let buttonTitle = sender.title(for: .normal) {
            //viewController.chatArr[0].quickReplyArr.removeAll()
            viewController.chatArr.insert(ChatData(message: buttonTitle, check_sending: 0), at: 0)
            viewController.tableView.reloadData()
            //viewController.addChatMsg(message: payload)
              viewController.addChatMsg(message: payload, lat: "", long: "", input_msg_type: Constants.shared.ITM_BUTTON)
            }
        }else{
            viewController.OpenFile(filePath: url, title: sender.title(for: .normal)!)
        }
    }
}

class ButtonsCVCell: UICollectionViewCell {
    
    var viewController: ChatVC!
    @IBOutlet weak var btnButtonTitle: CustomButton!
    
    var payload: String = ""
    var content_type: String = ""
    var url: String = ""

    override func awakeFromNib() {
        btnButtonTitle.setTitleColor(Constants.shared.hexStringToUIColor(hex: (Pref.getPref(key: Constants.shared.THEME_COLOR) as! String)), for: .normal)
        //backView.layer.cornerRadius = backView.bounds.height/2
      //pulseAnimation.startAnimating()
        //getSegmentTime()
    }
    
    @IBAction func btnButtonTap(_ sender: UIButton) {
        if content_type == Constants.shared.BTN_POSTBACK{
            postback(sender: sender)
        }else{
            viewController.OpenFile(filePath: url, title: sender.title(for: .normal)!)
        }
    }
    
    func postback(sender: UIButton){
        if let buttonTitle = sender.title(for: .normal) {
        //viewController.chatArr[0].quickReplyArr.removeAll()
        viewController.chatArr.insert(ChatData(message: buttonTitle, check_sending: 0), at: 0)
        viewController.tableView.reloadData()
        //viewController.addChatMsg(message: payload)
          viewController.addChatMsg(message: payload, lat: "", long: "", input_msg_type: Constants.shared.ITM_BUTTON)
        }
    }
}


extension ChatVC: UIWebViewDelegate{
    
    func OpenFile(filePath: String, title: String){
        
        webV = UIWebView()
        
        webV.frame = CGRect(x: 0, y: 0, width: UIScreen.main.bounds.width, height: UIScreen.main.bounds.height)
        webV.loadRequest(NSURLRequest(url: NSURL(string: filePath)! as URL) as URLRequest)
        webV.delegate = self
        webV.scrollView.setZoomScale(5, animated: true)
        self.view.addSubview(webV)
        
        /*let webView = WKWebView(frame: CGRect(x: 0, y: 0, width: self.view.frame.size.width, height: self.view.frame.size.height))
         self.view.addSubview(webView)
         let url = URL(string: filePath)
         webView.load(URLRequest(url: url!))*/
        
        let pdfVC = UIViewController()
        pdfVC.view.addSubview(webV)
        pdfVC.title = title
        self.navigationController?.pushViewController(pdfVC, animated: true)
        ProgressLoading.showLoading()
    }
    
    func webViewDidStartLoad(_ :UIWebView){
        print("abc")
    }
    func webViewDidFinishLoad(_ webView: UIWebView) {
        webV.scalesPageToFit = true
        ProgressLoading.hide()
    }
}
