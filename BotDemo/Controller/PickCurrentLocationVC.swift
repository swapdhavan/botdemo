//
//  PickCurrentLocationVC.swift
//  BOT
//
//  Created by Swapnil Dhavan on 01/11/19.
//  Copyright © 2019 Wind Hans Technologies. All rights reserved.
//

import UIKit
import GoogleMaps
import CoreLocation

class PickCurrentLocationVC: UIViewController, CLLocationManagerDelegate, GMSMapViewDelegate {

    @IBOutlet weak var btnClose: CustomButton!
    @IBOutlet weak var btnSendLocation: CustomButton!
    @IBOutlet weak var mapView: GMSMapView!
    var locationManager = CLLocationManager()
    var location: CLLocation!
    var viewController: ChatVC!
    var isLocationPicked = true
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
        noInternet()
        btnClose.backgroundColor = Constants.shared.hexStringToUIColor(hex: (Pref.getPref(key: Constants.shared.THEME_COLOR) as! String))
        btnSendLocation.backgroundColor = Constants.shared.hexStringToUIColor(hex: (Pref.getPref(key: Constants.shared.THEME_COLOR) as! String))
        
        //mapView?.center = self.view.center
        mapView.delegate = self
        mapView.reloadInputViews()
        
        mapView.isMyLocationEnabled = true
        mapView.settings.myLocationButton = true
        locationManager.desiredAccuracy = kCLLocationAccuracyBest
        locationManager.requestWhenInUseAuthorization()
        locationManager.startUpdatingLocation()
        locationManager.startMonitoringSignificantLocationChanges()
        locationManager.distanceFilter = 10
        locationManager.requestWhenInUseAuthorization()
        self.locationManager.delegate = self
    }
    
    func locationManager(_ manager: CLLocationManager, didChangeAuthorization status: CLAuthorizationStatus) {
        // 3
        guard status == .authorizedWhenInUse else {
            return
        }
        // 4
        locationManager.startUpdatingLocation()
        //5
        mapView.isMyLocationEnabled = true
        mapView.settings.myLocationButton = true
    }
    
    
    //Location Manager delegates
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        self.location = locations.last
        if let location = locations.last{
            if isLocationPicked{
            let camera = GMSCameraPosition.camera(withLatitude: location.coordinate.latitude, longitude: location.coordinate.longitude, zoom: 17.0, bearing: 0, viewingAngle: 0)
            self.mapView.animate(to: camera)
            isLocationPicked = false
            }
            /*mapView.clear()
            let position = CLLocationCoordinate2DMake(location.coordinate.latitude,location.coordinate.longitude)
            let marker = GMSMarker(position: position)
            marker.appearAnimation = .pop
            marker.map = mapView*/
        }
        //Finally stop updating location otherwise it will come again and again in this delegate
        //self.locationManager.stopUpdatingLocation()
    }
    @IBAction func btnCloseTap(_ sender: Any) {
        dismiss(animated: true, completion: nil)
    }
    @IBAction func btnSendLocationTap(_ sender: Any) {
        viewController.chatArr[0].quickReplyArr.removeAll()
        viewController.chatArr.insert(ChatData(message: "My Location", check_sending: 0), at: 0)
        viewController.tableView.reloadData()
        //viewController.SendLocation(lat: String(location.coordinate.latitude), long: String(location.coordinate.longitude))
        viewController.addChatMsg(message: "",lat: String(location.coordinate.latitude), long: String(location.coordinate.longitude), input_msg_type: Constants.shared.LOCATION)
        dismiss(animated: true, completion: nil)
    }
    
    func mapView(_ mapView: GMSMapView, idleAt position: GMSCameraPosition) {
        self.location = CLLocation(latitude: mapView.camera.target.latitude, longitude: mapView.camera.target.longitude)
            //getAddressFromLatLon(Latitude: String(mapView.camera.target.latitude), Longitude: String(mapView.camera.target.longitude), LocType: LocType)
        
        //print("Lat: ",mapView.camera.target.latitude)
        //print("Long: ",mapView.camera.target.longitude)
            
    }
}
