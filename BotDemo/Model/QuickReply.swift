//
//  Deligates.swift
//  DrawableExample
//
//  Created by Alap Soni on 09/10/18.
//  Copyright © 2018 Test. All rights reserved.
//

import Foundation
class QuickReply {
    var content_type: String = ""
    var title: String = ""
    var payload: String = ""
    
    init(json: JSON) {
        self.content_type = json["content_type"].stringValue
        if self.content_type == Constants.shared.LOCATION{
            self.title = "Pick Location"
        }else{
            self.title = json["title"].stringValue
        }
        self.payload = json["payload"].stringValue
    }
}
