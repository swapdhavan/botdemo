//
//  Deligates.swift
//  DrawableExample
//
//  Created by Alap Soni on 09/10/18.
//  Copyright © 2018 Test. All rights reserved.
//

import Foundation
class ChatData {
    var id: String = ""
    var message_type: String = ""
    var message: String = ""
    var template_type: String = ""
    var url: String = ""
    //var ride_id: String = ""
    var created_at: String = "sending..."
    var date = Date()
    var quickReplyArr: Array=[QuickReply]()
    var carouselArr: Array=[Caraousel]()
    var buttonArr: Array=[Buttons]()
    var titleHeight = 0
    var subTitleHeight = 0
    var buttonCount = 0
    
    init(message: String, check_sending: Int) {
        self.id = "1"
        self.message_type = Constants.shared.SENDER
        self.message = message
        self.template_type = Constants.shared.TEXT_MESSAGE
       
      /*  if check_sending == 1{
        let dateFormatter = DateFormatter()
        //dateFormatter.dateFormat = "yyyy-MM-dd HH:mm:ss"
        let created_at: Date = Date()
        dateFormatter.dateFormat = "dd MMM yyyy hh:mm aa"
        self.created_at = dateFormatter.string(from: created_at)
        }else{
            self.created_at = "sending..."
        }*/
    }
    
    init(json: JSON) {
        self.id = "2"
        self.message_type = Constants.shared.RECEIVER
    
        let message = JSON(json["message"])
        
        if message[Constants.shared.TEXT_MESSAGE].exists(){
            self.message = message[Constants.shared.TEXT_MESSAGE].stringValue
            self.template_type = Constants.shared.TEXT_MESSAGE
            if message[Constants.shared.QUICK_REPLIES].exists(){
        
                let quick_replies = message[Constants.shared.QUICK_REPLIES]
                for arr in quick_replies.arrayValue{
                    quickReplyArr.append(QuickReply(json: arr))
                }
            }
        }else if message["attachment"].exists(){
            let attachment = JSON(message["attachment"])
            
            if attachment["type"].stringValue == Constants.shared.IMAGE{
                let payload = JSON(attachment["payload"])
                self.url = payload["url"].stringValue
                self.template_type = Constants.shared.IMAGE
            }else if attachment["type"].stringValue == Constants.shared.VIDEO{
                let payload = JSON(attachment["payload"])
                self.url = payload["url"].stringValue
                self.template_type = Constants.shared.VIDEO
            }else if attachment["type"].stringValue == Constants.shared.ATT_TYPE{
                
                let payload = JSON(attachment["payload"])
                
                if payload["template_type"].stringValue == Constants.shared.CAROUSEL{
                self.template_type = Constants.shared.CAROUSEL
                let crArr = JSON(payload["elements"])
                for arr in crArr.arrayValue{
                    carouselArr.append(Caraousel(json: arr))
                    if titleHeight < Int(arr["title"].stringValue.height(withConstrainedWidth: 240)){
                        titleHeight = Int(arr["title"].stringValue.height(withConstrainedWidth: 240))
                    }
                    if subTitleHeight < Int(arr["subtitle"].stringValue.height(withConstrainedWidth: 240)){
                        subTitleHeight = Int(arr["subtitle"].stringValue.height(withConstrainedWidth: 240))
                    }
                    let buttons = JSON(arr["buttons"])
                    if buttonCount < buttons.arrayValue.count{
                        buttonCount = buttons.arrayValue.count
                    }
                }
                
                }else if payload["template_type"].stringValue == Constants.shared.BUTTONS{
                    self.template_type = Constants.shared.BUTTONS
                    self.message = payload["text"].stringValue
                    let btnArr = JSON(payload["buttons"])
                    for arr in btnArr.arrayValue{
                        buttonArr.append(Buttons(json: arr))
                    }
                }
            }
        }
         let dateFormatter = DateFormatter()
         //dateFormatter.dateFormat = "yyyy-MM-dd HH:mm:ss"
         let created_at: Date = Date()
         //dateFormatter.dateFormat = "dd MMM yyyy hh:mm aa"
         dateFormatter.dateFormat = "hh:mm aa"
         self.created_at = dateFormatter.string(from: created_at)
    }
    
    /*init(msg: String) {
        id = ""
        sender_id = ""
        receiver_id = ""
        message = msg
        ride_id = ""
        created_at = ""
    }*/
}
