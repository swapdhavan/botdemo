//
//  Deligates.swift
//  DrawableExample
//
//  Created by Alap Soni on 09/10/18.
//  Copyright © 2018 Test. All rights reserved.
//

import Foundation
class Buttons {
    var type: String = ""
    var title: String = ""
    var url: String = ""
    var payload: String = ""
    
    init(json: JSON) {
        self.type = json["type"].stringValue
        self.title = json["title"].stringValue
        
        if json["type"].stringValue == Constants.shared.BTN_POSTBACK{
            self.payload = json["payload"].stringValue
        }else{
            self.url = json["url"].stringValue
        }
    }
}
