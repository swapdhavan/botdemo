//
//  Deligates.swift
//  DrawableExample
//
//  Created by Alap Soni on 09/10/18.
//  Copyright © 2018 Test. All rights reserved.
//

import Foundation
class Caraousel {
    var title: String = ""
    var image_url: String = ""
    var subtitle: String = ""
    var buttonsArr: Array=[Buttons]()
    
    
    init(json: JSON) {
        self.title = json["title"].stringValue
        self.image_url = json["image_url"].stringValue
        self.subtitle = json["subtitle"].stringValue
        let buttons = JSON(json["buttons"])
        for arr in buttons.arrayValue{
            buttonsArr.append(Buttons(json: arr))
        }
    }
}
