Pod::Spec.new do |s|

# 1
s.platform = :ios
s.ios.deployment_target = '11.0'
s.name = "BotDemo"
s.summary = "Bot Demo Cocoa Pod creates for demo."
s.requires_arc = true

# 2
s.version = "0.1.1"

# 3
s.license = { :type => "MIT", :file => "LICENSE" }

# 4 - Replace with your name and e-mail address
s.author = { "Swapnil Dhavan" => "sdhavan04@gmail.com" }

# 5 - Replace this URL with your own GitHub page's URL (from the address bar)
s.homepage = "https://bitbucket.org/swapdhavan/botdemo/src"

# 6 - Replace this URL with your own Git URL from "Quick Setup"
s.source = { :git => "https://swapdhavan@bitbucket.org/swapdhavan/botdemo.git",
             :tag => "#{s.version}" }

# 7
s.framework = "UIKit"
s.dependency 'Alamofire', '~> 4.4'
s.dependency 'AlamofireImage', '~> 3.4'
s.dependency 'ISEmojiView'
s.dependency 'GoogleMaps'
s.dependency 'FLAnimatedImage', '~> 1.0'

# 8
s.source_files = "CocoaPodDemo/**/*.{swift}"

# 9
s.resources = "CocoaPodDemo/**/*.{png,jpeg,jpg,storyboard,xib,xcassets}"

# 10
s.swift_version = "4.2"

end
